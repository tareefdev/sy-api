const chemicalCollectionRaw = require("../data/chemical-collection.json");
const russianCollection = [];

// A temp solution until the data is ready to production
const chemicalCollection = chemicalCollectionRaw.data.filter(
  collection => collection.incident_date_time != null
);

function getCollection({ collection, lang }) {
  switch (collection) {
    case "chemical":
      return chemicalCollection.filter(collection => collection.lang == lang);
      break;
    case "russian":
      return russianCollection.filter(collection => collection.lang == lang);
      break;
    default:
      return [];
  }
}

module.exports = {
  get: getCollection
};
