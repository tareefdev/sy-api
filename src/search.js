const R = require("ramda");
var isBefore = require("date-fns/isBefore");
var isAfter = require("date-fns/isAfter");
const FlexSearch = require("flexsearch");
const ObservationsFile = require("../data/observations.json");
const arObservations = ObservationsFile.data.filter(
  ({ lang }) => lang === "ar"
);
const enObservations = [...ObservationsFile.data].filter(
  ({ lang }) => lang === "en"
);

const arIndex = new FlexSearch({
  encode: false,
  rtl: true,
  split: /\s+/,
  tokenize: "forward",
  doc: {
    id: "id",
    field: ["title", "incident_date_time", "location:name"]
  }
});

arIndex.add(arObservations);

const enIndex = new FlexSearch({
  encode: "balance",
  tokenize: "forward",
  doc: {
    id: "id",
    field: ["title", "location:name", "incident_date_time"]
  }
});

enIndex.add(enObservations);

function localizeSearch(searchQuery, lang) {
  const isEn = lang === "en";
  return isEn ? enIndex.search(searchQuery) : arIndex.search(searchQuery);
}

function filterbyDates(result, dateBefore, dateAfter) {
  const hasDateBefore = !R.isEmpty(dateBefore);
  const formatedDateBefore = new Date(dateBefore);
  const hasDateAfter = !R.isEmpty(dateAfter);
  const formatedDateAfter = new Date(dateAfter);
  let data = [...result];
  if (hasDateBefore) {
    data = data.filter(obj => {
      const date = new Date(obj.incident_date_time);
      return isBefore(date, formatedDateBefore);
    });
  }
  if (hasDateAfter) {
    data = data.filter(obj => {
      const date = new Date(obj.incident_date_time);
      return isAfter(date, formatedDateAfter);
    });
  }
  return data;
}

function searchObservations(searchQuery, otherOptions) {
  const { dateBefore, dateAfter, lang } = otherOptions;
  return filterbyDates(
    localizeSearch(searchQuery, lang),
    dateBefore,
    dateAfter
  );
}

module.exports = {
  search: searchObservations
};
